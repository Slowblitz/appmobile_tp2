package com.example.uapv1300579.tp2;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class BookDbHelper extends SQLiteOpenHelper {

    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";

    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";

    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + BookDbHelper.TABLE_NAME + " (" +
                    BookDbHelper._ID + " INTEGER PRIMARY KEY," +
                    BookDbHelper.COLUMN_BOOK_TITLE + " TEXT," +
                    BookDbHelper.COLUMN_AUTHORS + " TEXT," +
                    BookDbHelper.COLUMN_YEAR + " TEXT," +
                    BookDbHelper.COLUMN_GENRES + " TEXT," +
                    BookDbHelper.COLUMN_PUBLISHER + " TEXT)";


    @Override
    public void onCreate(SQLiteDatabase db) {

         db.execSQL(SQL_CREATE_ENTRIES) ;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(BookDbHelper.COLUMN_BOOK_TITLE , book.getTitle());
        values.put(BookDbHelper.COLUMN_AUTHORS , book.getAuthors());
        values.put(BookDbHelper.COLUMN_YEAR , book.getYear());
        values.put(BookDbHelper.COLUMN_GENRES , book.getGenres());
        values.put(BookDbHelper.COLUMN_PUBLISHER , book.getPublisher());

        // Inserting Row
        long rowID = 0;
        db.insert(TABLE_NAME,null,values);
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    public int updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();



        ContentValues values = new ContentValues();
        values.put(BookDbHelper.COLUMN_BOOK_TITLE , book.getTitle());
        values.put(BookDbHelper.COLUMN_AUTHORS , book.getAuthors());
        values.put(BookDbHelper.COLUMN_YEAR , book.getYear());
        values.put(BookDbHelper.COLUMN_GENRES , book.getGenres());
        values.put(BookDbHelper.COLUMN_PUBLISHER , book.getPublisher());
        int res = 0;
        db.update(TABLE_NAME,values,"_id="+book.getId(),null);
        // updating row
        // call db.update()
        return res;
    }


    public Cursor fetchAllBooks() {
        SQLiteDatabase db = this.getReadableDatabase();

        try{
        Cursor cursor = db.rawQuery("select * from "+ TABLE_NAME ,null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }
    catch(Exception e){
            return null ;

    }
    }

    public void deleteBook(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
        int id=cursor.getColumnIndexOrThrow("_id");
        long id2 = cursor.getLong(id);
        db.delete(TABLE_NAME,_ID+ "="+ String.valueOf(id),null);
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public static Book cursorToBook(Cursor cursor) {
        Book book = null;
        int idIndex= cursor.getColumnIndexOrThrow("_id");
        int title= cursor.getColumnIndexOrThrow("title");
        int author= cursor.getColumnIndexOrThrow("authors");
        int year= cursor.getColumnIndexOrThrow("year");
        int genre= cursor.getColumnIndexOrThrow("genres");
        int pub= cursor.getColumnIndexOrThrow("publisher");

        long id = cursor.getLong(idIndex);
        String name = cursor.getString(title);
        String author1  = cursor.getString(author);
        String years =cursor.getString(year);
        String gender = cursor.getString(genre);
        String publisher = cursor.getString(pub);

        book =new Book(id,name,author1,years,gender,publisher);

        return book;
    }
}
