package com.example.uapv1300579.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BookDbHelper bookdb = new BookDbHelper(this) ;
        bookdb.populate();
        Cursor cursor = bookdb .fetchAllBooks();
        String [] col = new  String[]{BookDbHelper.COLUMN_BOOK_TITLE,BookDbHelper.COLUMN_AUTHORS};


            final ListView listview = (ListView) findViewById(R.id.ListB);
        SimpleCursorAdapter adapter =new SimpleCursorAdapter(this,android.R.layout.simple_list_item_2,
                cursor,
                col,new int[]{android.R.id.text1,android.R.id.text2});

        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent= new Intent(MainActivity.this, Main2Activity.class);
                Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                Book book =BookDbHelper.cursorToBook(cursor);
                intent.putExtra("book",book);
                startActivity(intent);
            }
        });
          //  final BookDbHelper listbook1= new BookDbHelper();
           // final String values = listbook1.getDatabaseName();
          //  final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Collections.singletonList(values));
            //listview.setAdapter(adapter);


        }
    }

