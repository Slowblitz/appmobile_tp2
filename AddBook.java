package com.example.uapv1300579.tp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddBook extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        Button sauvegarder = (Button) findViewById(R.id.sauve);

        sauvegarder.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                //Récupère les données écrites dans la textView
                EditText editName = (EditText) findViewById(R.id.nameBook);
                EditText editAuthors = (EditText) findViewById(R.id.editAuthors);
                EditText editYear = (EditText) findViewById(R.id.editYear);
                EditText editGenre = (EditText) findViewById(R.id.editGenres);
                EditText editPublisher = (EditText) findViewById(R.id.editPublisher);


                //On caste les donées récupéré en leurs type d'origine
                String name = editName.getText().toString();
                String author = editAuthors.getText().toString();
                String year = editYear.getText().toString();
                String genre = editGenre.getText().toString();
                String publisher = editPublisher.getText().toString();

                //Si toutes les champs sont bien remplies
                if (!name.isEmpty() && !author.isEmpty())
                {
                    Book book = new Book(0,name, author, year, genre, publisher);
                    BookDbHelper bookDbHelper = new BookDbHelper(AddBook.this);
                    bookDbHelper.addBook(book);

                    Toast.makeText(AddBook.this, "Sauvegarde effectué",
                            Toast.LENGTH_LONG).show();


                    Intent intent1 = new Intent(AddBook.this,MainActivity.class) ;
                    startActivity(intent1);
                    finish();
                }
                //Si un champs n'est pas remplit
                else
                {
                    Toast.makeText(AddBook.this, "Le titre n'est pas renseigné",
                            Toast.LENGTH_LONG).show();
                }
            }

            ;
        });
    }
}